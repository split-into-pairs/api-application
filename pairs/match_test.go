package pairs

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/split-into-pairs/api-application/employees"
)

func TestMatchEmployees_SingleElement(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(0, "A", "A", "A", 20, false),
	}

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 1)
}

func TestMatchEmployees_TwoPairs(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(0, "A", "A", "B", 20, false),
		employees.CreateEmployee(1, "B", "A", "A", 21, true),
		employees.CreateEmployee(2, "C", "B", "A", 21, true),
		employees.CreateEmployee(3, "D", "B", "B", 20, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[0] = 2
	expectedResult[1] = 3
	expectedResult[2] = 0
	expectedResult[3] = 1

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 2, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d (ids: %d, %d)", i, r.First.ID, r.Second.ID)
	}
}

func TestMatchEmployees_ThreePairs(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(0, "A", "A", "A", 20, false),
		employees.CreateEmployee(1, "A", "B", "A", 21, false),
		employees.CreateEmployee(2, "B", "A", "B", 21, false),
		employees.CreateEmployee(3, "B", "B", "B", 22, false),
		employees.CreateEmployee(4, "C", "B", "A", 24, false),
		employees.CreateEmployee(5, "C", "C", "B", 25, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[0] = 3
	expectedResult[1] = 5
	expectedResult[2] = 4
	expectedResult[3] = 0
	expectedResult[4] = 2
	expectedResult[5] = 1

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 3, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d", i)
	}
}

func TestMatchEmployees_FourPairs(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(0, "A", "A", "A", 20, false),
		employees.CreateEmployee(1, "A", "B", "A", 21, false),
		employees.CreateEmployee(2, "B", "A", "B", 21, false),
		employees.CreateEmployee(3, "B", "B", "B", 22, false),
		employees.CreateEmployee(4, "C", "B", "A", 24, false),
		employees.CreateEmployee(5, "C", "C", "B", 25, false),
		employees.CreateEmployee(6, "C", "D", "E", 26, false),
		employees.CreateEmployee(7, "C", "G", "F", 27, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[0] = 3
	expectedResult[1] = 5
	expectedResult[2] = 4
	expectedResult[3] = 0
	expectedResult[4] = 2
	expectedResult[5] = 1
	expectedResult[6] = 7
	expectedResult[7] = 6

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 4, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d", i)
	}
}

func TestMatchEmployees_SortMatching(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(1, "A", "A", "A", 25, false),
		employees.CreateEmployee(2, "B", "B", "B", 24, false),
		employees.CreateEmployee(3, "C", "C", "A", 30, false),
		employees.CreateEmployee(4, "D", "C", "C", 21, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[1] = 4
	expectedResult[2] = 3
	expectedResult[3] = 2
	expectedResult[4] = 1

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 2, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d", i)
	}
}

func TestMatchEmployees_SortMatching_MiddleElement(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(1, "A", "A", "A", 25, false),
		employees.CreateEmployee(2, "B", "B", "B", 24, false),
		employees.CreateEmployee(4, "D", "C", "C", 21, false),
		employees.CreateEmployee(3, "C", "C", "A", 30, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[1] = 4
	expectedResult[2] = 3
	expectedResult[3] = 2
	expectedResult[4] = 1

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 2, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d", i)
	}
}

func TestMatchEmployees_SortMatching_EmptyElement(t *testing.T) {
	testData := []employees.Employee{
		employees.CreateEmployee(1, "A", "A", "A", 25, false),
		employees.CreateEmployee(2, "B", "A", "A", 25, false),
		employees.CreateEmployee(3, "C", "A", "A", 25, false),
		employees.CreateEmployee(4, "D", "B", "B", 24, false),
	}
	expectedResult := make(map[int]int)
	expectedResult[1] = 4
	expectedResult[2] = 0
	expectedResult[3] = 0
	expectedResult[4] = 1

	result := CreateSolver().Solve(testData)
	assert.Len(t, result, 3, stringWithPairs(result))

	for i, r := range result {
		assert.Equal(t, expectedResult[r.First.ID], r.Second.ID, "failed example no %d", i)
	}
}

func stringWithPairs(result []Pair) string {
	p := []string{}
	for _, r := range result {
		p = append(p, fmt.Sprintf("pair %d %d", r.First.ID, r.Second.ID))
	}

	return strings.Join(p, ", ")
}
