package pairs

import (
	"gitlab.com/split-into-pairs/api-application/employees"
)

type Solution interface {
	Solve(all []employees.Employee) []Pair
}
type solver struct {
	all       []employees.Employee
	alwaysAll []employees.Employee
	pairs     []Pair
	maximum   int
	maxPairs  []Pair
}

func (s solver) Solve(all []employees.Employee) []Pair {
	s.all = all
	s.alwaysAll = all
	s.maximum = 0

	return s.makeSolution().missing().maxPairs
}

func CreateSolver() Solution {
	return solver{}
}

func (s solver) makeSolution() solver {
	for _, e := range s.all {
		for _, a := range s.all {
			if e.ID == a.ID {
				continue
			}

			if s.areEmployeesMatching(e, a) {
				s.pairs = append(s.pairs, Pair{e, a})
				s.all = removeFromCollection(e, s.all)
				s.all = removeFromCollection(a, s.all)

				if s.maximum < len(s.pairs) {
					s.maximum = len(s.pairs)
					s.maxPairs = s.pairs
				}

				s = s.makeSolution()
				if s.isSolved() {
					return s
				}

				s.all = insertIntoCollection(e, s.all)
				s.all = insertIntoCollection(a, s.all)
				s.pairs = removePair(e, a, s.pairs)
			}
		}
	}

	return s
}

func (s solver) missing() solver {
	used := make(map[int]employees.Employee)
	for _, u := range s.maxPairs {
		used[u.First.ID] = u.First
		used[u.Second.ID] = u.Second
	}

	for _, e := range s.alwaysAll {
		if _, ok := used[e.ID]; !ok {
			s.maxPairs = append(s.maxPairs, Pair{First: e})
		}
	}

	return s
}

func (s solver) isSolved() bool {
	count := len(s.alwaysAll)
	if count%2 == 1 {
		count = count - 1
	}

	elements := make(map[int]employees.Employee)
	for _, p := range s.pairs {
		elements[p.First.ID] = p.First
		elements[p.Second.ID] = p.Second
	}

	return len(elements) == count
}

func (s solver) areEmployeesMatching(f, a employees.Employee) bool {
	isMatching, _ := composedSpec()(f, a)
	return isMatching
}

func composedSpec() specification {
	return composeSpecifications(
		isDifferentAge,
		isLivingInAnotherDistrict,
		areBothNotHaveBadEyesight,
		belongsToDifferentTeam,
	)
}

func removeFromCollection(e employees.Employee, all []employees.Employee) []employees.Employee {
	var tmp []employees.Employee
	for _, a := range all {
		if a.ID != e.ID {
			tmp = append(tmp, a)
		}
	}

	return tmp
}

func removePair(e, a employees.Employee, all []Pair) []Pair {
	var tmp []Pair
	for _, p := range all {
		if (p.First.ID == e.ID || p.Second.ID == e.ID) && (p.First.ID == a.ID || p.Second.ID == a.ID) {
			continue
		}

		tmp = append(tmp, p)
	}

	return tmp
}

func insertIntoCollection(e employees.Employee, all []employees.Employee) []employees.Employee {
	return append(all, e)
}
