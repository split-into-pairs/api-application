package pairs

import "gitlab.com/split-into-pairs/api-application/employees"

func isDifferentAge(f, s employees.Employee) (bool, int) {
	return f.Age != s.Age, 1
}

func belongsToDifferentTeam(f, s employees.Employee) (bool, int) {
	return f.TeamName != s.TeamName, 1
}

func isLivingInAnotherDistrict(f, s employees.Employee) (bool, int) {
	return f.DistrictName != s.DistrictName, 1
}

func areBothNotHaveBadEyesight(f, s employees.Employee) (bool, int) {
	return (f.Eyesight == false && s.Eyesight == false) || (f.Eyesight != s.Eyesight), 1
}

func composeSpecifications(specs ...specification) specification {
	return func(f, s employees.Employee) (bool, int) {
		var countTrue int

		for _, spec := range specs {
			isTrue, _ := spec(f, s)
			if isTrue {
				countTrue++
			}
		}

		if countTrue == len(specs) {
			return true, countTrue
		}

		return false, countTrue
	}
}
