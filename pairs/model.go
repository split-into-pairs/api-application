package pairs

import (
	"gitlab.com/split-into-pairs/api-application/employees"
)

type specification func(f, s employees.Employee) (bool, int)

type Pair struct {
	First  employees.Employee `json:"first"`
	Second employees.Employee `json:"second"`
}

func (p Pair) isSatisfiedBy(s specification) bool {
	isTrue, _ := s(p.First, p.Second)
	return isTrue
}
