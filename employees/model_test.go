package employees

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CreateEmployee(t *testing.T) {
	e := CreateEmployee(1, "Lorem ipsum", "A", "001", 25, true)
	assert.Equal(t, "Lorem ipsum", e.Name)
}
