package employees

import (
	"github.com/jinzhu/gorm"
)

type FindAll func() ([]Employee, error)
type Find func(id int) (Employee, error)
type Persist func(e Employee) error
type Remove func(e Employee) error

type Repository interface {
	FindAll() ([]Employee, error)
	Find(id int) (Employee, error)
	Persist(e Employee) error
	Remove(e Employee) error
}

type mysqlRepository struct {
	db *gorm.DB
}

func (r mysqlRepository) FindAll() ([]Employee, error) {
	var employees []Employee
	err := r.db.Find(&employees).Error

	return employees, err
}
func (r mysqlRepository) Find(id int) (Employee, error) {
	var employee Employee
	err := r.db.Find(&employee, "id = ?", id).Error

	return employee, err
}
func (r mysqlRepository) Persist(e Employee) error {
	return r.db.Save(&e).Error
}
func (r mysqlRepository) Remove(e Employee) error {
	return r.db.Delete(&e).Error
}

func CreateRepository(db *gorm.DB) Repository {
	db.AutoMigrate(&Employee{})

	return &mysqlRepository{
		db: db,
	}
}
