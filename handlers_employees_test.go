package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/split-into-pairs/api-application/employees"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func errorTestingContext(t *testing.T) func(c *gin.Context) {
	return func(c *gin.Context) {
		c.Next()
		assert.Len(t, c.Errors, 0)
	}
}

func Test_GetEmployeesHandler_OK(t *testing.T) {
	findAll := func() ([]employees.Employee, error) {
		return []employees.Employee{}, nil
	}

	r := gin.New()
	r.GET("/", errorTestingContext(t), GetEmployeesHandler(findAll))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
}

func Test_GetEmployeesHandler_ErrorResNotFound(t *testing.T) {
	findAll := func() ([]employees.Employee, error) {
		return []employees.Employee{}, errors.New("cannot fetch employees")
	}

	r := gin.New()
	r.GET("/", GetEmployeesHandler(findAll))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_GetSingleEmployeeHandler_OK(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.Employee{}, nil
	}

	r := gin.New()
	r.GET("/:id", errorTestingContext(t), GetSingleEmployeeHandler(find))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
}

func Test_GetSingleEmployeeHandler_ErrorIDType(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.Employee{}, nil
	}

	r := gin.New()
	r.GET("/:id", GetSingleEmployeeHandler(find))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/hello", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func Test_GetSingleEmployeeHandler_ErrorResNotFound(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.Employee{}, errors.New("cannot find employee")
	}

	r := gin.New()
	r.GET("/:id", GetSingleEmployeeHandler(find))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_SaveEmployeeHandler_OK(t *testing.T) {
	emp, err := json.Marshal(employees.CreateEmployee(1, "test", "test", "test", 12, true))
	assert.NoError(t, err)

	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.POST("/", errorTestingContext(t), SaveEmployeeHandler(save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/", bytes.NewReader(emp))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusCreated, w.Code)
}

func Test_SaveEmployeeHandler_ErrorBinding(t *testing.T) {
	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.POST("/", SaveEmployeeHandler(save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/", strings.NewReader(
		"{\"name\": \"Lorem ipsum\"}",
	))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_SaveEmployeeHandler_ErrorOnSave(t *testing.T) {
	emp, err := json.Marshal(employees.CreateEmployee(1, "test", "test", "test", 12, true))
	assert.NoError(t, err)

	save := func(e employees.Employee) error {
		return errors.New("cannot save employee")
	}

	r := gin.New()
	r.POST("/", SaveEmployeeHandler(save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/", bytes.NewReader(emp))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_UpdateEmployeeHandler_OK(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.PUT("/:id", errorTestingContext(t), UpdateEmployeeHandler(find, save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/1", strings.NewReader(
		"{\"age\": 25}",
	))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusCreated, w.Code)
}

func Test_UpdateEmployeeHandler_ErrorIDType(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.PUT("/:id", UpdateEmployeeHandler(find, save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/hello", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func Test_UpdateEmployeeHandler_ResNotFound(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.Employee{}, errors.New("employee not found")
	}
	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.PUT("/:id", UpdateEmployeeHandler(find, save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_UpdateEmployeeHandler_ErrorBinding(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	save := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.PUT("/:id", UpdateEmployeeHandler(find, save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/1", strings.NewReader(
		"{\"age\": \"hello\"}",
	))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_UpdateEmployeeHandler_ErrorOnSave(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	save := func(e employees.Employee) error {
		return errors.New("cannot save")
	}

	r := gin.New()
	r.PUT("/:id", UpdateEmployeeHandler(find, save))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/1", strings.NewReader(
		"{\"age\": 25}",
	))
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_RemoveEmployeeHandler_OK(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	remove := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.DELETE("/:id", errorTestingContext(t), RemoveEmployeeHandler(find, remove))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodDelete, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusAccepted, w.Code)
}

func Test_RemoveEmployeeHandler_ErrorIDType(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	remove := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.DELETE("/:id", RemoveEmployeeHandler(find, remove))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodDelete, "/hello", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func Test_RemoveEmployeeHandler_ResNotFound(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.Employee{}, errors.New("not found")
	}
	remove := func(e employees.Employee) error {
		return nil
	}

	r := gin.New()
	r.DELETE("/:id", RemoveEmployeeHandler(find, remove))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodDelete, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func Test_RemoveEmployeeHandler_ErrorOnRemove(t *testing.T) {
	find := func(id int) (employees.Employee, error) {
		return employees.CreateEmployee(1, "A", "B", "C", 24, false), nil
	}
	remove := func(e employees.Employee) error {
		return errors.New("cannot remove")
	}

	r := gin.New()
	r.DELETE("/:id", RemoveEmployeeHandler(find, remove))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodDelete, "/1", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
}
