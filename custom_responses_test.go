package main

import (
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_InternalServerError(t *testing.T) {
	code, _ := internalServerError(errors.New("test"))
	assert.Equal(t, http.StatusInternalServerError, code)
}

func Test_BadRequestError(t *testing.T) {
	code, _ := badRequestError(errors.New("html"))
	assert.Equal(t, http.StatusBadRequest, code)
}

func Test_OKWithBody(t *testing.T) {
	code, _ := okWithData("test")
	assert.Equal(t, http.StatusOK, code)
}

func Test_EmptyContent(t *testing.T) {
	code := emptyResponse()
	assert.Equal(t, http.StatusNoContent, code)
}
