package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/split-into-pairs/api-application/employees"
	"gitlab.com/split-into-pairs/api-application/pairs"
)

func ConnectPairs(findAll employees.FindAll, solve pairs.Solution) gin.HandlerFunc {
	return func(c *gin.Context) {
		all, err := findAll()
		if err != nil {
			c.Status(emptyResponse())
			return
		}

		p := solve.Solve(all)
		if len(p) == 0 {
			c.Status(emptyResponse())
			return
		}

		c.JSON(okWithData(p))
	}
}
